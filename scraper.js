'use strict'
let testUrl = 'http://www.scoot.co.uk/contact-us';
let cmd = require('node-cmd');
let YamlPath = "spider.yaml";
let yaml = require('js-yaml');
let fs   = require('fs');
//let htmlToJson = require('html-to-json');
let cheerio = require('cheerio');
let _= require('lodash');

let fetchHtml = url=> {

	return new Promise ((fulfill, reject)=> {

		//get the html content -> command, callback
		cmd.get('curl '+url, (error, standardOut, standarsErr) => {

			if (standarsErr){
				console.log(standarsErr);
			}
			if (standardOut){
				fulfill(standardOut);
			}
			if (error){
				reject(new Error(error));
			}
		})
	});
}

let fetchYamlFile = path => {

	return new Promise ((fulfill, reject)=> {

		let yamlFile = yaml.safeLoad(fs.readFileSync(path, 'utf8'));

		if (yamlFile) {
			fulfill(yamlFile);

		} else {
			reject(new Error('Not a valid file or path'));
		}
	});
};

let onReject = error => {
	console.log("Scraping HTML failed because: \n",error.message);
}

let scrape = (values) => {

	let yaml = values[1];
	let $ = cheerio.load(values[0]);
	

	return new Promise ((fulfill, reject)=> {

		let itemsList = {};
		for (let item in yaml) {

			itemsList[item] = [];

			 $('html').find(yaml[item]['path'].join(" > ")).each((index,element) => {

	              if (!_.isUndefined(element.children) && _.isArray(element.children) && !_.isEmpty(element.children)) {

	                for (let child in element.children) {
	                    itemsList[item].push(element.children[child].data);
	                }
	              }

	           });
	
		}
		
		if (!_.isEmpty(itemsList)) {

			fulfill(itemsList);

		} else {
			reject (new Error ('Could not scrape anything because result array is empty'));
		}

	});

}

let parseHtml = html => {

  return new Promise((fulfill, reject) => {
    try {
      fulfill(cheerio.load(html));
    } catch (e) {
      reject(new Error ("Can not parse html"));
    }

  })
}

Promise.all([fetchHtml(testUrl), fetchYamlFile(YamlPath)]).then(scrape).then(console.log).catch(onReject);
