
# README #


### What is dsari-spider? ###

* it's just a scraper.
* Version 1.0.0

### How to use ###

* Just do "node scraper.js" on terminal.

### What am I using? ###
 
 - I'm using 'node-cmd', 'js-yaml', 'fs', 'cheerio' and 'lodash'.
 - The url used is 'http://www.scoot.co.uk/contact-us'.
 - Yamle file's path is "spider.yaml" and it's hard coded and relative.
 
